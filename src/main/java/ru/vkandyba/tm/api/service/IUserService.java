package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.User;

import java.util.List;

public interface IUserService extends IRepository<User> {

    User findByLogin(String login);

    User removeUser(User user);

    User removeByLogin(String login);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User create(String login, String password);

    User updateUser(String userId, String firstName, String lastName, String middleName);

    User setPassword(String userId, String password);

    boolean isLoginExists(String login);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);

}
