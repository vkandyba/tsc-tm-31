package ru.vkandyba.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;

public class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks...";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CLEAR TASKS]");
        serviceLocator.getTaskService().clear(userId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
