package ru.vkandyba.tm.command.data;

import javassist.tools.rmi.ObjectNotFoundException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.dto.Domain;
import ru.vkandyba.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    protected static final String FILE_BINARY = "./data.bin";

    protected static final String FILE_BASE64 = "./data.base64";

    protected static final String FILE_JSON_FASTERXML = "./data-fasterxml.json";

    protected static final String FILE_JSON_JAXB = "./data-jaxb.json";

    protected static final String FILE_XML_FASTERXML = "./data-fasterxml.xml";

    protected static final String FILE_XML_JAXB = "./data-jaxb.xml";

    protected static final String FILE_YAML_FASTERXML = "./data-fasterxml.yaml";

    protected static final String JAVAX_XML_BIND_CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    protected static final String ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    protected static final String ECLIPSELINK_MEDIA_TYPE = "eclipselink.media-type";

    protected static final String APPLICATION_JSON = "application/json";

    protected static final String BACKUP_FILE_NAME = "backup.xml";

    @NotNull
    @SneakyThrows
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    @SneakyThrows
    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

    @Nullable
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
